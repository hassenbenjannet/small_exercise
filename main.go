package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	f "tayara/functions"
)

type whoami struct {
	Name  string
	Title string
	State string
}

func main() {
	request1()
}

func whoAmI(response http.ResponseWriter, r *http.Request) {
	who := []whoami{
		whoami{Name: "Michael Levan",
			Title: "Developer Advocate",
			State: "NJ",
		},
	}

	json.NewEncoder(response).Encode(who)

	fmt.Println("Endpoint Hit", who)
}

func homePage(response http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(response, "Welcome to the Go Web API !!! \n")
	map_user_info := f.GetUserInfo("+21629470816")
	user := ""
	for key, element := range map_user_info {
		user += key + " : " + element + "\n"
		// fmt.Println(key, "=>", element)
	}
	json.NewEncoder(response).Encode(user)

	fmt.Println(user)
	// fmt.Fprintf(response, string(user))

	// f.ChangeUserPass(map_user_info["profile_id"], "aazz1122")

	fmt.Println("Endpoint Hit: homePage")
}

func aboutMe(response http.ResponseWriter, r *http.Request) {
	who := "Ben jannet Hsan"

	fmt.Fprintf(response, "A little bit about Ben jannet Hsan...")
	fmt.Println("Endpoint Hit: ", who)
}
func doNothing(w http.ResponseWriter, r *http.Request) {}

func request1() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/aboutme", aboutMe)
	http.HandleFunc("/whoami", whoAmI)
	http.HandleFunc("/favicon.ico", doNothing)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
