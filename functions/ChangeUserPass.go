package functions

import (
	"bytes"
	"fmt"
	"net/http"
)

func ChangeUserPass(profile_id string, new_pass string) string {
	httpposturl := "https://www.tayara.tn/iam/identity.Identity/ChangeUserPassword"

	fmt.Println("URL:>", httpposturl)

	var jsonStr = []byte(string(0) + string(0) + string(0) + string(0) + "0" + string(10) + profile_id + string(18) + string(8) + new_pass)

	req, err := http.NewRequest("POST", httpposturl, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/grpc-web+proto")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	return new_pass
}
