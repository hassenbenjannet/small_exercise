package functions

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func GetUserInfo(phone string) map[string]string {
	httpposturl := "https://www.tayara.tn/core/marketplace.MarketPlace/GetUserByPhoneNumber"
	var result map[string]string
	result = make(map[string]string)
	fmt.Println("URL:>", httpposturl)

	var jsonStr = []byte(string(0) + string(0) + string(0) + string(0) + string(14) + string(10) + string(12) + phone)

	req, err := http.NewRequest("POST", httpposturl, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/grpc-web+proto")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	body_str := string(body)
	if body_str == "" {
		return result
	}
	// fmt.Println("response Body:", body_str)

	result["profile_id"] = body_str[35:72]
	info_chunk := body_str[92:200]
	result["info"] = GetStringInBetween(info_chunk, "+")

	return result
}

func GetStringInBetween(str string, end string) (result string) {

	e := strings.Index(str, end)
	if e == -1 {
		return
	}
	e += 12
	return str[0:e]
}
