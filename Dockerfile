FROM golang:latest

RUN mkdir /build
WORKDIR /build

RUN cd /build && git clone https://hassenbenjannet:utS6yzJNu4D2gJ39KpFC@bitbucket.org/hassenbenjannet/small_exercise.git

RUN cd /build/small_exercise/main && go build main.go

EXPOSE 8080

ENTRYPOINT [ "/build/small_exercise/main/main" ]